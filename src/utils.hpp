#pragma once
#include <iostream>
#include <unistd.h>

#define NUM_SECS 3

void Wait(int secs)
{

    sleep(secs);
}

class ErrorHandler
{
public:
    void operator()(int error_code)
    {
        std::cout << "error_code " << error_code << std::endl;
    }
};

template <typename T>
void AnalyzeConsumers(T &consumers)
{
    int appr_msgs_in_sec = 0;
    int32_t errors_num = 0;
    int32_t errors_num_in_sec = 0;
    int64_t appr_total_receive_msgs_in_sec = 0;
    for (auto &consumer : consumers)
    {
        //PrintInfo(consumer);
        appr_msgs_in_sec += consumer.get()->GetLastValue() / NUM_SECS;
        appr_total_receive_msgs_in_sec += consumer.get()->TotalMessages() / NUM_SECS;

        errors_num += consumer.get()->GetNumErrors();
    }
    errors_num_in_sec = errors_num / NUM_SECS;
    std::cout << "approximately rec messages    : " << appr_total_receive_msgs_in_sec << " / sec" << std::endl;
    std::cout << "approximately all messages    : " << appr_msgs_in_sec << " / sec" << std::endl;
    std::cout << "approximately errors          : " << errors_num_in_sec << " / sec" << std::endl;
    std::cout << "approximately errors total    : " << errors_num << std::endl;
}

struct Value64
{
    int32_t value_{0};
    int32_t value1_{0};
    int64_t value2_{0};
    int64_t value3_{0};
    int64_t value4_{0};
    int64_t value5_{0};
    int64_t value6_{0};
    int64_t value7_{0};
    int64_t value8_{0};

    Value64(int32_t value) : value_(value) {}

    /*Value64 &operator++(int)
    {
        this->value_++;
        return *this;
    }*/
};

struct Value128
{
    Value64 value_{0};
    Value64 value1_{0};

    Value128(int32_t value)
    {
    }
};

struct Value256
{
    Value128 value_{0};
    Value128 value1_{0};

    Value256(int32_t value)
    {
    }
};

struct Value512
{
    Value256 value_{0};
    Value256 value1_{0};

    Value512(int32_t value)
    {
    }
};

struct Value1024
{
    Value512 value_{0};
    Value512 value1_{0};

    Value1024(int32_t value)
    {
    }
};

struct Value2048
{
    Value1024 value_{0};
    Value1024 value1_{0};

    Value2048(int32_t value)
    {
    }
};

struct Value4096
{
    Value2048 value_{0};
    Value2048 value1_{0};

    Value4096(int32_t value)
    {
    }
};

struct Value8192
{
    Value4096 value_{0};
    Value4096 value1_{0};

    Value8192(int32_t value)
    {
    }
};