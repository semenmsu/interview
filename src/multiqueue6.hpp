#pragma once

#pragma once
#include <map>
#include <list>
#include <thread>
#include <mutex>
#include <functional>
#include <unistd.h>
#include <iostream>
#include <atomic>
#include <condition_variable>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <queue>

#define MaxCapacity 20000

//https://stackoverflow.com/questions/45507041/how-to-check-if-weak-ptr-is-empty-non-assigned
template <typename T>
bool is_uninitialized(std::weak_ptr<T> const &weak)
{
    using wt = std::weak_ptr<T>;
    return !weak.owner_before(wt{}) && !wt{}.owner_before(weak);
}

//thread-safe EventQueue
//guarantee MaxCapacity for every Queue
template <typename Key, typename Value, typename Queue>
class GuaranteeEveryMaxCapacityQueue
{
public:
    GuaranteeEveryMaxCapacityQueue()
    {
    }

    void Enqueue(Key id, Value value)
    {

        std::lock_guard<std::recursive_mutex> lock{mtx_};
        //std::cout << "enqueu " << size_ << std::endl;
        auto iter = queues_.find(id);
        if (iter != queues_.end())
        {
            if (iter->second.size() < MaxCapacity)
            {
                iter->second.push(value);
                size_++;
                //updated_keys_.insert(id);
                updated_queues_.push(id);
                //return true;
            }
            else
            {
                exceed_capacity_++;
            }
        }
        else
        {
            queues_.insert(std::make_pair(id, Queue()));
            iter = queues_.find(id);
            if (iter != queues_.end())
            {
                if (iter->second.size() < MaxCapacity)
                {
                    iter->second.push(value);
                    size_++;
                    updated_queues_.push(id);
                    //return true;
                }
                else
                {
                    exceed_capacity_++;
                }
            }
        }

        cv_.notify_one();
        //return false;
    }

    Value Dequeue(Key id)
    {
        auto iter = queues_.find(id);
        if (iter != queues_.end())
        {
            if (iter->second.size() > 0)
            {
                auto front = iter->second.front();
                iter->second.pop();
                size_--;
                return front; //copy, should optimize?
            }
        }

        return Value{};
    }

    //good for coroutines?
    std::unique_ptr<std::pair<Key, Value>> Poll()
    {
        std::unique_lock<std::recursive_mutex> lock{mtx_};
        if (!size_)
        {
            cv_.wait(lock, [this]() { return !size_; });
        }

        if (size_)
        {
            auto id = updated_queues_.front();
            updated_queues_.pop();
            auto value = Dequeue(id);
            auto ptr = std::make_unique<std::pair<Key, Value>>(id, value);
            return ptr;
        }

        return std::unique_ptr<std::pair<Key, Value>>(nullptr);
    }

    bool empty()
    {
        return !size_;
    }

    int32_t size()
    {
        return size_;
    }

    int32_t exceed_capacity()
    {
        return exceed_capacity_;
    }

    std::unordered_set<Key> updated_keys_;
    int32_t size_{0};

protected:
    std::recursive_mutex mtx_;
    std::condition_variable_any cv_;
    std::map<Key, Queue> queues_;
    std::queue<Key> updated_queues_;
    int32_t exceed_capacity_{0};
};

//отделяем Consumers
template <typename Key, typename Value>
class ConsumerManager
{
public:
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        std::cout << "try subscribe id = " << id << std::endl;
        std::lock_guard<std::mutex> lock(mtx);
        if (!consumers.count(id))
        {
            consumers.emplace(id, consumer);
        }
    }

    void Unsubscribe(Key id)
    {
        std::lock_guard<std::mutex> lock(mtx);
        if (consumers.count(id))
        {
            consumers.erase(id);
        }
    }

    void Push(Key id, Value &value)
    {
        std::unique_lock<std::mutex> lock(mtx);

        if (!consumers.count(id))
        {
            return;
        }
        auto consumer_ptr = consumers[id].lock();

        //lock.unlock(); ?? под вопросом

        if (consumer_ptr)
        {

            consumer_ptr->Consume(id, value);
        }
    }

protected:
    std::unordered_map<Key, std::weak_ptr<IConsumer<Key, Value>>> consumers;
    std::mutex mtx;
};

/*
struct StreamHandler
{
    StreamHandler(StreamHandler const &) = default;
    void operator()(int32_t) {}
};

using ErrorHandler = StreamHandler;*/

template <typename Key, typename Value>
class MultiQueueProcessor
{
public:
    MultiQueueProcessor() : running_{true},
                            th(std::bind(&MultiQueueProcessor::Process, this)) {}

    MultiQueueProcessor(std::function<void(int)> error_handler) : error_handler_(error_handler), running_{true},
                                                                  th(std::bind(&MultiQueueProcessor::Process, this)) {}

    ~MultiQueueProcessor()
    {
        std::cout << "exceed capacity value " << queue_.exceed_capacity() << std::endl;
        StopProcessing();
        th.join();
    }

    void StopProcessing()
    {
        running_ = false;
    }

    //сделать как в примере, интерфейс не меняем
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        manager_.Subscribe(id, consumer);
    }

    void Unsubscribe(Key id)
    {
        manager_.Unsubscribe(id);
    }

    void Enqueue(Key id, Value value)
    {
        queue_.Enqueue(id, value);
    }

protected:
    void Process()
    {
        while (running_)
        {
            auto pair = queue_.Poll();
            if (pair.get())
            {
                manager_.Push(pair->first, pair->second);
            }
        }
        if (error_handler_)
        {
            error_handler_(0);
        }
    }

protected:
    bool running_;
    std::thread th;
    GuaranteeEveryMaxCapacityQueue<Key, Value, std::queue<Value>> queue_;
    ConsumerManager<Key, Value> manager_;
    std::function<void(int)> error_handler_;
};