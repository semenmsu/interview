#pragma once
#include <unistd.h>
#include <assert.h>

template <typename Key, typename Value>
struct IConsumer
{
    virtual void Consume(Key id, const Value &value)
    {
        id;
        value;
    }
};

template <typename Key, typename Value>
class Consumer : public IConsumer<Key, Value>
{
public:
    Consumer(Key id) : id_(id)
    {
    }
    void Consume(Key id, const Value &value) override
    {
        assert(id == id_);

        if ((value - last_value_) != 1)
        {
            num_errors_++;
        }
        last_value_ = value;
    }

    Key GetLastValue()
    {
        return last_value_;
    }

    int32_t GetNumErrors()
    {
        return num_errors_;
    }

private:
    Value last_value_{0};
    int32_t num_errors_{0};
    Key id_;
};