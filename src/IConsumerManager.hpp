#pragma once
#include "IConsumer.hpp"
#include <memory>

template <typename Key, typename Value>

struct IConsumerManager
{
    virtual void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer) = 0;
    virtual void Unsubscribe(Key id) = 0;
    virtual bool TryPush(Key id, Value &value) = 0;
};
