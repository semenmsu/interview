#pragma once

#pragma once
#include <map>
#include <list>
#include <thread>
#include <mutex>
#include <functional>
#include <unistd.h>
#include <iostream>
#include <atomic>
#include <condition_variable>
#include <unordered_set>
#include <unordered_map>
#include <memory>

#define MaxCapacity 10000

//https://stackoverflow.com/questions/45507041/how-to-check-if-weak-ptr-is-empty-non-assigned
template <typename T>
bool is_uninitialized(std::weak_ptr<T> const &weak)
{
    using wt = std::weak_ptr<T>;
    return !weak.owner_before(wt{}) && !wt{}.owner_before(weak);
}

//thread-safe EventQueue
//guarantee MaxCapacity for every Queue
template <typename Key, typename Value>
class GuaranteeEveryMaxCapacityQueue
{
public:
    GuaranteeEveryMaxCapacityQueue()
    {
    }

    bool Enqueue(Key id, Value value)
    {

        auto iter = queues.find(id);
        if (iter != queues.end())
        {
            if (iter->second.size() < MaxCapacity)
            {
                iter->second.push_back(value);
                size_++;
                updated_keys_.insert(id);
                return true;
            }
            else
            {
                //std::cout << "queue id " << id << " exceed capacity" << std::endl;
                exceed_capacity_++;
            }
        }
        else
        {
            //std::cout << "create new queue" << std::endl;
            queues.insert(std::make_pair(id, std::list<Value>()));
            iter = queues.find(id);
            if (iter != queues.end())
            {
                if (iter->second.size() < MaxCapacity)
                {
                    iter->second.push_back(value);
                    size_++;
                    return true;
                }
                else
                {
                    std::cout << "queue id " << id << " exceed capacity" << std::endl;
                    exceed_capacity_++;
                }
            }
        }
        return false;
    }

    Value Dequeue(Key id)
    {
        auto iter = queues.find(id);
        if (iter != queues.end())
        {
            if (iter->second.size() > 0)
            {
                auto front = iter->second.front();
                iter->second.pop_front();
                size_--;
                return front;
            }
        }
        return Value{};
    }

    bool empty()
    {
        return !size_;
    }

    int32_t size()
    {
        return size_;
    }

    int32_t exceed_capacity()
    {
        return exceed_capacity_;
    }

    std::unordered_set<Key> updated_keys_;
    int32_t size_{0};

protected:
    std::map<Key, std::list<Value>> queues;

    int32_t exceed_capacity_{0};
};

//отделяем Consumers
template <typename Key, typename Value>
class ConsumerManager
{
public:
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        std::cout << "try subscribe id = " << id << std::endl;
        std::lock_guard<std::mutex> lock(mtx);
        if (!consumers.count(id))
        {
            consumers.emplace(id, consumer);
            //consumers.insert(std::make_pair(id, consumer));
        }
    }

    void Unsubscribe(Key id)
    {
        std::lock_guard<std::mutex> lock(mtx);
        if (consumers.count(id))
        {
            consumers.erase(id);
        }
    }

    void Push(Key id, Value &value)
    {
        //std::cout << "Push " << std::endl;

        std::unique_lock<std::mutex> lock(mtx);

        if (!consumers.count(id))
        {
            return;
        }
        auto consumer_ptr = consumers[id].lock();

        //lock.unlock(); ?? под вопросом

        if (consumer_ptr)
        {

            consumer_ptr->Consume(id, value);
        }

        /*
        if (is_uninitialized(consumer))
        {
            auto consumer_ptr = consumer.lock();
            if (consumer_ptr)
            {
                consumer_ptr->Consume(id, value);
            }
        }*/
    }


protected:
    std::unordered_map<Key, std::weak_ptr<IConsumer<Key, Value>>> consumers;
    std::mutex mtx;
};

template <typename Key, typename Value>
class MultiQueueProcessor
{
public:
    MultiQueueProcessor() : running{true}, running_{true},
                            th(std::bind(&MultiQueueProcessor::Process, this)) {}

    ~MultiQueueProcessor()
    {
        std::cout << "exceed capacity value " << exceed_capacity_ << std::endl;
        StopProcessing();
        th.join();
    }

    void StopProcessing()
    {
        running_ = false;
        //std::cout << "Stop processing running" << running_ << std::endl;
        cv.notify_one();
    }

    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        /*
        std::lock_guard<std::recursive_mutex> lock{mtx};
        auto iter = consumers.find(id);
        if (iter == consumers.end())
        {
            consumers.insert(std::make_pair(id, consumer));
        }*/
        manager_.Subscribe(id, consumer);
    }

    void Unsubscribe(Key id)
    {
        /*
        std::lock_guard<std::recursive_mutex> lock{mtx};
        auto iter = consumers.find(id);
        if (iter != consumers.end())
            consumers.erase(id);*/
        manager_.Unsubscribe(id);
    }

    void Enqueue(Key id, Value value)
    {
        std::lock_guard<std::recursive_mutex> lock{mtx};
        queue_.Enqueue(id, value);
        cv.notify_one();
    }

protected:
    void Process()
    {
        while (running_)
        {
            std::unique_lock<std::recursive_mutex> lock{mtx};
            cv.wait(lock, [this]() { return !queue_.empty() || !running_; });
            if (running_)
            {
                for (auto &key : queue_.updated_keys_)
                {
                    Value front;
                    while ((front = queue_.Dequeue(key)) != Value{})
                    {
                        //consumerIter->second->Consume(key, front);
                        manager_.Push(key, front);
                    }

                    /*
                    auto consumerIter = consumers.find(key);
                    if (consumerIter != consumers.end())
                    {
                        Value front;
                        while ((front = queue_.Dequeue(key)) != Value{})
                        {
                            //consumerIter->second->Consume(key, front);
                            manager_.Push(key, front);
                        }
                    }*/
                }
                queue_.updated_keys_.clear();
            }
        }
    }

protected:
    std::map<Key, IConsumer<Key, Value> *> consumers;
    std::map<Key, std::list<Value>> queues;
    std::unordered_set<Key> updated_keys_;

    bool running;
    std::atomic_bool running_;
    std::recursive_mutex mtx;
    std::thread th;
    int32_t exceed_capacity_{0};
    //std::atomic_int32_t size_{0};
    int32_t size_;
    std::condition_variable_any cv;
    GuaranteeEveryMaxCapacityQueue<Key, Value> queue_;
    ConsumerManager<Key, Value> manager_;
};