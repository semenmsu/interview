//#include "producer.hpp"
//#include "consumer.hpp"
//#include "multiqueue1.hpp"
//#include "multiqueue2.hpp"
//#include "multiqueue3.hpp"
//#include "multiqueue4.hpp"
//#include "multiqueue5.hpp"
//#include "multiqueue6.hpp"
//#include "multiqueue7.hpp"
#include "multiqueue9.hpp"
#include <memory>
#include <thread>
#include <vector>
#include "utils.hpp"
#include "test/SimpleProducersConsumersScenario.hpp"
#include <random>

template <typename Key, typename Value>
class ConsumersBag
{
public:
    ConsumersBag(MultiQueueProcessor<Key, Value> &mqp, int32_t max_consumers) : mqp_(mqp), max_consumers_(max_consumers)
    {
        for (int i = 0; i < max_consumers_; i++)
        {
            consumers_.emplace(i, std::make_shared<Consumer<Key, Value>>(i));
        }
    }
    void Run()
    {
        for (int i = 0; i < max_consumers_; i++)
        {
            mqp_.Subscribe(i, consumers_[i]);
        }
        running_ = true;
        std::thread th(std::bind(&ConsumersBag::Process, this));
        th_ = std::move(th);
    }

    void Process()
    {
        while (running_)
        {
            sleep(1);
        }
    }

    void Analyze()
    {
        std::list<std::shared_ptr<Consumer<Key, Value>>> consumers;
        for (auto &kvp : consumers_)
        {
            consumers.push_back(kvp.second);
        }
        AnalyzeConsumers(consumers);
    }

    void Stop()
    {
        running_ = false;
    }

    ~ConsumersBag()
    {
        Stop();
        if (th_.joinable())
        {
            th_.join();
        }
    }

private:
    std::map<int32_t, std::shared_ptr<Consumer<Key, Value>>> consumers_;
    int32_t max_consumers_{0};
    MultiQueueProcessor<Key, Value> &mqp_;
    bool running_{false};
    std::thread th_;
};

template <typename Key, typename Value>
class RandomSubscribeConsumersBag
{
public:
    RandomSubscribeConsumersBag(MultiQueueProcessor<Key, Value> &mqp, int32_t max_consumers, int32_t delay) : mqp_(mqp), max_consumers_(max_consumers), delay_{delay}
    {
        for (int i = 0; i < max_consumers_; i++)
        {
            consumers_.emplace(i, std::make_shared<Consumer<Key, Value>>(i));
        }
    }

    void Run()
    {
        /*for (int i = 0; i < max_consumers_; i++)
        {
            mqp_.Subscribe(i, consumers_[i]);
        }*/
        running_ = true;
        std::thread th(std::bind(&RandomSubscribeConsumersBag::Process, this));
        th_ = std::move(th);
    }

    void Process()
    {
        std::vector<int32_t> unsubscribed;
        std::vector<int32_t> subscribed;
        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
        std::uniform_real_distribution<> uniform_real(0.0, 1.0);
        std::uniform_int_distribution<> uniform_int(0, max_consumers_ - 1);
        double subscribe_probability = 0.5;
        double unsubscribe_probability = 0.3;

        for (int i = 0; i < max_consumers_; i++)
        {
            unsubscribed.push_back(i);
        }
        while (running_)
        {
            //try subscribe with probability p
            if (subscribed.size() < max_consumers_ && uniform_real(gen) < subscribe_probability)
            {
                std::uniform_int_distribution<> subscribe_distribution(0, unsubscribed.size() - 1);
                auto index = subscribe_distribution(gen);
                auto value = unsubscribed[index];
                //std::cout << "size" << unsubscribed.size() << std::endl;
                //std::cout << "index" << index << std::endl;
                //std::cout << "subscribe " << value << std::endl;
                unsubscribed.erase(unsubscribed.begin() + index);

                subscribed.push_back(value);
                mqp_.Subscribe(value, consumers_[value]);
            }

            //try unsubscribe with probability q
            if (subscribed.size() > 0 && uniform_real(gen) < unsubscribe_probability)
            {
                std::uniform_int_distribution<> unsubscribe_distribution(0, subscribed.size() - 1);
                auto index = unsubscribe_distribution(gen);
                auto value = subscribed[index];
                subscribed.erase(subscribed.begin() + index);
                unsubscribed.push_back(value);
                mqp_.Unsubscribe(value);
            }

            /*std::cout << "subscribed: ";
            for (auto &s : subscribed)
            {
                std::cout << s << " ";
            }
            std::cout << std::endl;

            std::cout << std::endl;*/
            usleep(delay_);
        }
    }

    void Analyze()
    {
        std::list<std::shared_ptr<Consumer<Key, Value>>> consumers;
        for (auto &kvp : consumers_)
        {
            consumers.push_back(kvp.second);
        }
        AnalyzeConsumers(consumers);
    }

    void Stop()
    {
        running_ = false;
    }

    ~RandomSubscribeConsumersBag()
    {
        Stop();
        if (th_.joinable())
        {
            th_.join();
        }
    }

private:
    std::map<int32_t, std::shared_ptr<Consumer<Key, Value>>> consumers_;
    int32_t max_consumers_{0};
    MultiQueueProcessor<Key, Value> &mqp_;
    bool running_{false};
    std::thread th_;
    int32_t delay_;
};

template <typename Key, typename Value>
class RandomDeleteConsumerBag
{
};

template <typename Key, typename Value>
class ProducersBag
{
public:
    ProducersBag(MultiQueueProcessor<Key, Value> &mqp, int32_t max_producers, int32_t delay) : mqp_(mqp), max_producers_(max_producers), delay_{delay}
    {
        for (int i = 0; i < max_producers_; i++)
        {
            //consumers_.emplace(i, std::make_shared<Consumer<int32_t, int32_t>>(i));
            producers_.push_back(std::make_unique<Producer<Key, Value, MultiQueueProcessor<Key, Value>>>(mqp_, i, 1, delay_));
        }
    }

    void Run()
    {
        //roducer.get()->StartProducing();
        for (auto &producer : producers_)
        {
            producer.get()->StartProducing();
        }
    }

private:
    int32_t max_producers_;
    std::vector<std::unique_ptr<Producer<Key, Value, MultiQueueProcessor<Key, Value>>>> producers_;
    MultiQueueProcessor<Key, Value> &mqp_;
    bool running_{false};
    std::thread th_;
    int32_t delay_;
};

//
template <typename Value>
void RandomSubscribeUnsusbscribeScenario(int32_t consumers_num, int32_t producer_delay)
{

    MultiQueueProcessor<int32_t, Value> multiQueueProcessor;
    RandomSubscribeConsumersBag<int32_t, Value> consumers(multiQueueProcessor, consumers_num, 10'000);
    {
        ProducersBag<int32_t, Value> producers(multiQueueProcessor, consumers_num, producer_delay);
        Wait(1);
        producers.Run();
        consumers.Run();
        Wait(NUM_SECS);
        consumers.Stop();
    }
    Wait(0);
    consumers.Analyze();
}

int main()
{
    //SimpleProducersConsumersScenario();
    RandomSubscribeUnsusbscribeScenario<int32_t>(50, 0);
    return 0;
}