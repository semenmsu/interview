#pragma once

#include "ConsumerManager.hpp"
#include <queue>

template <typename Key, typename Value, typename Queue>
class EventQueue
{
public:
    EventQueue()
    {
    }

    void Enqueue(Key id, Value value)
    {

        std::lock_guard<std::recursive_mutex> lock{mtx_};
        auto iter = queues_.find(id);
        if (iter != queues_.end())
        {
            if (iter->second.size() < MaxCapacity)
            {
                iter->second.push(value);
                updated_queues_.push(id);
                size_++;
            }
            else
            {
                //std::cout << "Exceed Queue " << id << std::endl;
                exceed_capacity_++;
            }
        }
        else
        {
            queues_.insert(std::make_pair(id, Queue()));
            iter = queues_.find(id);
            if (iter != queues_.end())
            {
                if (iter->second.size() < MaxCapacity)
                {
                    iter->second.push(value);
                    updated_queues_.push(id);
                    size_++;
                }
                else
                {
                    exceed_capacity_++;
                }
            }
        }

        cv_.notify_one();
    }

    void PersistRoutine(ConsumerManager<Key, Value> &manager)
    {
        //check waiting queues, если появилась возможность то скидываем
        std::vector<int> vec;
        for (auto id : waiting_queues_)
        {
            auto iter = queues_.find(id);
            if (iter != queues_.end())
            {
                while (iter->second.size())
                {
                    auto front = iter->second.front();

                    if (manager.TryPush(id, front))
                    {
                        iter->second.pop(); //удаляем из очереди
                    }
                    else
                    {
                        break;
                    }
                }

                if (iter->second.size() == 0)
                {
                    vec.push_back(id);
                }
            }
        }
        for (auto id : vec)
        {
            waiting_queues_.erase(id);
        }
    }

    void Poll(ConsumerManager<Key, Value> &manager)
    {

        std::unique_lock<std::recursive_mutex> lock{mtx_};

        if (!size_)
        {
            cv_.wait_for(lock, 10ms, [this]() { return size_; });
            //waiting_queues_.size();
        }

        if constexpr (Persist)
        {
            PersistRoutine(manager);
        }

        while (size_)
        {

            auto id = updated_queues_.front();
            updated_queues_.pop();
            size_--; //общей очереди
            assert(size_ >= 0);
            //inline Dequeue
            auto iter = queues_.find(id);
            if (iter != queues_.end())
            {
                if (iter->second.size())
                {
                    auto front = iter->second.front();
                    if constexpr (!Persist)
                    {
                        manager.TryPush(id, front);
                        iter->second.pop();
                    }
                    else
                    {
                        if (manager.TryPush(id, front))
                        {
                            iter->second.pop();
                        }
                        else
                        {
                            waiting_queues_.insert(id);
                        }
                    }
                }
            }
        }
    }

    bool empty()
    {
        return !size_;
    }

    int32_t size()
    {
        return size_;
    }

    int32_t exceed_capacity()
    {
        return exceed_capacity_;
    }

    std::unordered_set<Key> updated_keys_;
    int32_t size_{0};

protected:
    std::recursive_mutex mtx_;
    std::condition_variable_any cv_;
    std::map<Key, Queue> queues_;
    std::queue<Key> updated_queues_;
    std::unordered_set<Key> waiting_queues_;
    int32_t exceed_capacity_{0};
    const bool persist_{true};
};