#include <thread>
#include <iostream>
#include <queue>
#include <mutex>
#include <condition_variable>

std::mutex mx;
std::condition_variable cv;
std::queue<int> q;

bool finished = false;

void producer(int n)
{
    for (int i = 0; i < n; ++i)
    {
        {
            std::lock_guard<std::mutex> lk(mx);
            q.push(i);
            //std::cout << "pushing " << i << std::endl;
        }
        cv.notify_all();
    }
    {
        std::lock_guard<std::mutex> lk(mx);
        finished = true;
    }
    cv.notify_all();
}

void consumer()
{
    while (true)
    {
        std::unique_lock<std::mutex> lk(mx);
        cv.wait(lk, [] { return finished || !q.empty(); });
        while (!q.empty())
        {
            //std::cout << "consuming " << q.front() << std::endl;
            q.pop();
        }
        if (finished)
            break;
    }
}
#define ITERS 4'000'000

int main()
{
    auto start = std::chrono::system_clock::now();

    std::thread t1(producer, ITERS);
    std::thread t2(consumer);
    t1.join();
    t2.join();

    std::cout << "finished!" << std::endl;
    auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    auto mill = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << elapsed.count() << " nano" << '\n';
    std::cout << mill.count() << " ms" << '\n';
}