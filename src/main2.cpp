#include "producer.hpp"
#include "consumer.hpp"
//#include "multiqueue1.hpp"
//#include "multiqueue2.hpp"
//#include "multiqueue3.hpp"
#include "multiqueue4.hpp"

#include <vector>

void Wait(int secs)
{

    sleep(secs);
}

#define NUM_SECS 2
void scenario1()
{
    std::cout << "**************       scenario 1   *************" << std::endl;
    Consumer<int32_t, int32_t> consumer;
    {
        MultiQueueProcessor<int32_t, int32_t> mqp;
        std::vector<Producer<int32_t, int32_t, decltype(mqp)>> p;
        Producer<int32_t, int32_t, decltype(mqp)> producer1(mqp, 2, 0);
        mqp.Subscribe(2, &consumer);
        Wait(NUM_SECS);
    }
    std::cout << "last_value: " << consumer.GetLastValue() << std::endl;
    std::cout << "approximately:" << consumer.GetLastValue() / NUM_SECS << std::endl
              << std::endl;
}

void scenario2()
{
    std::cout << "**************       scenario 2   *************" << std::endl;
    Consumer<int32_t, int32_t> consumer;
    {
        MultiQueueProcessor<int32_t, int32_t> mqp;
        std::vector<Producer<int32_t, int32_t, decltype(mqp)>> p;
        Producer<int32_t, int32_t, decltype(mqp)> producer1(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> producer2(mqp, 2, 0);
        mqp.Subscribe(2, &consumer);
        Wait(NUM_SECS);
    }
    std::cout << "last_value: " << consumer.GetLastValue() << std::endl;
    std::cout << "approximately:" << consumer.GetLastValue() / NUM_SECS << std::endl
              << std::endl;
}

void scenario3()
{
    std::cout << "**************       scenario 3   *************" << std::endl;
    Consumer<int32_t, int32_t> consumer;
    {
        MultiQueueProcessor<int32_t, int32_t> mqp;
        std::vector<Producer<int32_t, int32_t, decltype(mqp)>> p;
        Producer<int32_t, int32_t, decltype(mqp)> producer1(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> producer2(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer3(mqp, 2, 0);
        mqp.Subscribe(2, &consumer);
        Wait(NUM_SECS);
    }
    std::cout << "last_value: " << consumer.GetLastValue() << std::endl;
    std::cout << "approximately:" << consumer.GetLastValue() / NUM_SECS << std::endl
              << std::endl;
}

void scenario4()
{
    std::cout << "**************       scenario 4   *************" << std::endl;
    Consumer<int32_t, int32_t> consumer;
    {
        MultiQueueProcessor<int32_t, int32_t> mqp;
        std::vector<Producer<int32_t, int32_t, decltype(mqp)>> p;
        Producer<int32_t, int32_t, decltype(mqp)> producer1(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> producer2(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer3(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer4(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer5(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer6(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer7(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer8(mqp, 2, 0);
        mqp.Subscribe(2, &consumer);
        Wait(NUM_SECS);
    }
    std::cout << "last_value: " << consumer.GetLastValue() << std::endl;
    std::cout << "approximately:" << consumer.GetLastValue() / NUM_SECS << std::endl
              << std::endl;
}

int main()
{
    scenario1();
    //scenario2();
    //scenario3();
    //scenario4();
    return 0;
}