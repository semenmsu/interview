#include "producer.hpp"
#include "consumer.hpp"
#include "multiqueue1.hpp"

void Wait(int secs)
{

    sleep(secs);
}

#define NUM_SECS 5
int main()
{
    Consumer<int32_t, int32_t> consumer;
    {
        MultiQueueProcessor<int32_t, int32_t> mqp;
        //Producer<int32_t, int32_t> producer(mqp, 2, 0);
        //Producer<int32_t, int32_t, MultiQueueProcessor<int32_t, int32_t>> Producer(mqp, 2, 0);
        Producer<int32_t, int32_t, decltype(mqp)> Producer(mqp, 2, 0);
        mqp.Subscribe(2, &consumer);
        Wait(NUM_SECS);
    }
    std::cout << "last_value: " << consumer.GetLastValue() << std::endl;
    std::cout << "approximately:" << consumer.GetLastValue() / NUM_SECS << std::endl;
    return 0;
}