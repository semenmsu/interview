#pragma once

#include <iostream>
#include <thread>
#include <functional>
#include <unistd.h>

template <typename Key, typename Value, typename Processor>
class Producer
{
public:
    //Producer(Processor &mqp, Key key, Value start_value) : mqp_(mqp), key_{key}, value_{start_value}, th_(std::bind(&Producer::Produce, this)) {}
    Producer(Processor &mqp, Key key, Value start_value) : mqp_(mqp), key_{key}, value_{start_value} {}

    Producer(Processor &mqp, Key key, Value start_value, int32_t delay) : mqp_(mqp), key_{key}, value_{start_value}, delay_{delay} {}

    void Produce()
    {
        while (running_)
        {
            usleep(delay_);
            mqp_.Enqueue(key_, value_++);
        }
    }

    Producer(const Producer &other) = delete;

    Producer(Producer &&other) = delete;

    void StartProducing()
    {
        std::thread th(std::bind(&Producer::Produce, this));
        th_ = std::move(th);
        running_ = true;
    }

    void StopProducing()
    {
        running_ = false;
    }

    ~Producer()
    {
        StopProducing();
        if (th_.joinable())
        {
            th_.join();
        }
    }

private:
    bool running_{false};
    std::thread th_;
    Processor &mqp_;
    Key key_;
    Value value_;
    int32_t delay_{0};
};
