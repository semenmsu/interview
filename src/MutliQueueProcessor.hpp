#pragma once
#include "EventQueue.hpp"
#include <functional>

template <typename Key, typename Value>
class MultiQueueProcessor
{
public:
    MultiQueueProcessor() : running_{true},
                            th_(std::bind(&MultiQueueProcessor::Process, this)) {}

    MultiQueueProcessor(std::function<void(int)> error_handler) : error_handler_(error_handler), running_{true},
                                                                  th_(std::bind(&MultiQueueProcessor::Process, this)) {}

    ~MultiQueueProcessor()
    {
        //std::cout << "exceed capacity value " << queue_.exceed_capacity() << std::endl;
        StopProcessing();
        th_.join();
    }

    void StopProcessing()
    {
        running_ = false;
    }

    //сделать как в примере, интерфейс не меняем
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        std::cout << "Subscribe " << id << std::endl;
        manager_.Subscribe(id, consumer);
    }

    void Unsubscribe(Key id)
    {
        std::cout << "Unsubscribe " << id << std::endl;
        manager_.Unsubscribe(id);
    }

    void Enqueue(Key id, Value value)
    {
        queue_.Enqueue(id, value);
    }

protected:
    void Process()
    {
        while (running_)
        {
            queue_.Poll(manager_);
        }
    }

protected:
    bool running_;
    std::thread th_;
    EventQueue<Key, Value, std::queue<Value>> queue_;
    ConsumerManager<Key, Value> manager_;
    std::function<void(int)> error_handler_;
};