#pragma once
template <typename Key, typename Value>
class Producer
{
public:
    Producer(MultiQueueProcessor<Key, Value> &mqp, Key key, Value start_value) : mqp_(mqp), key_{key}, value_{start_value}, running_(true), th_(std::bind(&Producer::Produce, this)) {}

    void Produce()
    {
        while (running_)
        {
            //std::cout << " produce" << std::endl;
            //usleep(1000);
            //sleep(0);
            mqp_.Enqueue(key_, value_++);
        }
    }

    void StopProducing()
    {
        running_ = false;
    }

    ~Producer()
    {
        StopProducing();
        th_.join();
    }

private:
    std::thread th_;
    bool running_;
    MultiQueueProcessor<Key, Value> &mqp_;
    Key key_;
    Value value_;
};