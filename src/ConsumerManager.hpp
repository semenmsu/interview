#pragma once
#include "IConsumerManager.hpp"

#include <map>
#include <thread>
#include <mutex>
#include <memory>
#include <unordered_map>
#include <chrono>

using namespace std::chrono_literals;

#define MaxCapacity 100
#define Persist true

template <typename Key, typename Value>
class ConsumerManager
{
public:
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        std::lock_guard<std::mutex> lock(mtx);
        if (!consumers.count(id))
        {
            consumers.emplace(id, consumer);
        }
    }

    void Unsubscribe(Key id)
    {
        std::lock_guard<std::mutex> lock(mtx);
        if (consumers.count(id))
        {
            consumers.erase(id);
        }
    }

    bool TryPush(Key id, Value &value)
    {
        std::unique_lock<std::mutex> lock(mtx);

        auto iter = consumers.find(id);
        if (iter == consumers.end())
        {
            return false;
        }
        lock.unlock();
        auto consumer_ptr = iter->second.lock();
        if (consumer_ptr)
        {

            consumer_ptr->Consume(id, value);

            return true;
        }
        return false;
    }

protected:
    std::unordered_map<Key, std::weak_ptr<IConsumer<Key, Value>>> consumers;
    std::mutex mtx;
};