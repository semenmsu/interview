#pragma once

#pragma once
#include <map>
#include <list>
#include <thread>
#include <mutex>
#include <functional>
#include <unistd.h>
#include <iostream>
#include <atomic>
#include <condition_variable>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <queue>
#include "IConsumer.hpp"
#include <type_traits>
#include <assert.h>
#include <chrono>

using namespace std::chrono_literals;

#define MaxCapacity 100
#define Persist true

template <typename Key, typename Value>
class ConsumerManager
{
public:
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        std::lock_guard<std::mutex> lock(mtx);
        if (!consumers.count(id))
        {
            consumers.emplace(id, consumer);
        }
    }

    void Unsubscribe(Key id)
    {
        std::lock_guard<std::mutex> lock(mtx);
        if (consumers.count(id))
        {
            consumers.erase(id);
        }
    }

    bool TryPush(Key id, Value &value)
    {
        std::unique_lock<std::mutex> lock(mtx);

        auto iter = consumers.find(id);
        if (iter == consumers.end())
        {
            return false;
        }
        lock.unlock();
        auto consumer_ptr = iter->second.lock();
        if (consumer_ptr)
        {

            consumer_ptr->Consume(id, value);

            return true;
        }
        return false;
    }

protected:
    std::unordered_map<Key, std::weak_ptr<IConsumer<Key, Value>>> consumers;
    std::mutex mtx;
};

template <typename Key, typename Value, typename Queue>
class GuaranteeEveryMaxCapacityQueue
{
public:
    GuaranteeEveryMaxCapacityQueue()
    {
    }

    void Enqueue(Key id, Value value)
    {

        std::lock_guard<std::recursive_mutex> lock{mtx_};
        auto iter = queues_.find(id);
        if (iter != queues_.end())
        {
            if (iter->second.size() < MaxCapacity)
            {
                iter->second.push(value);
                updated_queues_.push(id);
                size_++;
            }
            else
            {
                //std::cout << "Exceed Queue " << id << std::endl;
                exceed_capacity_++;
            }
        }
        else
        {
            queues_.insert(std::make_pair(id, Queue()));
            iter = queues_.find(id);
            if (iter != queues_.end())
            {
                if (iter->second.size() < MaxCapacity)
                {
                    iter->second.push(value);
                    updated_queues_.push(id);
                    size_++;
                }
                else
                {
                    exceed_capacity_++;
                }
            }
        }

        cv_.notify_one();
    }

    void PersistRoutine(ConsumerManager<Key, Value> &manager)
    {
        //check waiting queues, если появилась возможность то скидываем
        std::vector<int> vec;
        for (auto id : waiting_queues_)
        {
            auto iter = queues_.find(id);
            if (iter != queues_.end())
            {
                while (iter->second.size())
                {
                    auto front = iter->second.front();

                    if (manager.TryPush(id, front))
                    {
                        iter->second.pop(); //удаляем из очереди
                    }
                    else
                    {
                        break;
                    }
                }

                if (iter->second.size() == 0)
                {
                    vec.push_back(id);
                }
            }
        }
        for (auto id : vec)
        {
            waiting_queues_.erase(id);
        }
    }

    void Poll(ConsumerManager<Key, Value> &manager)
    {

        std::unique_lock<std::recursive_mutex> lock{mtx_};

        if (!size_)
        {
            cv_.wait_for(lock, 10ms, [this]() { return size_; });
            //waiting_queues_.size();
        }

        if constexpr (Persist)
        {
            PersistRoutine(manager);
        }

        while (size_)
        {

            auto id = updated_queues_.front();
            updated_queues_.pop();
            size_--; //общей очереди
            assert(size_ >= 0);
            //inline Dequeue
            auto iter = queues_.find(id);
            if (iter != queues_.end())
            {
                if (iter->second.size())
                {
                    auto front = iter->second.front();
                    if constexpr (!Persist)
                    {
                        manager.TryPush(id, front);
                        iter->second.pop();
                    }
                    else
                    {
                        if (manager.TryPush(id, front))
                        {
                            iter->second.pop();
                        }
                        else
                        {
                            waiting_queues_.insert(id);
                        }
                    }
                }
            }
        }
    }

    bool empty()
    {
        return !size_;
    }

    int32_t size()
    {
        return size_;
    }

    int32_t exceed_capacity()
    {
        return exceed_capacity_;
    }

    std::unordered_set<Key> updated_keys_;
    int32_t size_{0};

protected:
    std::recursive_mutex mtx_;
    std::condition_variable_any cv_;
    std::map<Key, Queue> queues_;
    std::queue<Key> updated_queues_;
    std::unordered_set<Key> waiting_queues_;
    int32_t exceed_capacity_{0};
    const bool persist_{true};
};

template <typename Key, typename Value>
class MultiQueueProcessor
{
public:
    MultiQueueProcessor() : running_{true},
                            th_(std::bind(&MultiQueueProcessor::Process, this)) {}

    MultiQueueProcessor(std::function<void(int)> error_handler) : error_handler_(error_handler), running_{true},
                                                                  th_(std::bind(&MultiQueueProcessor::Process, this)) {}

    ~MultiQueueProcessor()
    {
        //std::cout << "exceed capacity value " << queue_.exceed_capacity() << std::endl;
        StopProcessing();
        th_.join();
    }

    void StopProcessing()
    {
        running_ = false;
    }

    //сделать как в примере, интерфейс не меняем
    void Subscribe(Key id, std::shared_ptr<IConsumer<Key, Value>> consumer)
    {
        std::cout << "Subscribe " << id << std::endl;
        manager_.Subscribe(id, consumer);
    }

    void Unsubscribe(Key id)
    {
        std::cout << "Unsubscribe " << id << std::endl;
        manager_.Unsubscribe(id);
    }

    void Enqueue(Key id, Value value)
    {
        queue_.Enqueue(id, value);
    }

protected:
    void Process()
    {
        while (running_)
        {
            queue_.Poll(manager_);
        }
    }

protected:
    bool running_;
    std::thread th_;
    GuaranteeEveryMaxCapacityQueue<Key, Value, std::queue<Value>> queue_;
    ConsumerManager<Key, Value> manager_;
    std::function<void(int)> error_handler_;
};