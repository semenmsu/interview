/*std::vector<int32_t> unsubscribed;
    std::vector<int32_t> subscribed;
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> uniform_real(0.0, 1.0);
    std::uniform_int_distribution<> uniform_int(0, consumers_num - 1);
    double subscribe_probability = 0.5;
    double unsubscribe_probability = 0.3;

    for (int i = 0; i < consumers_num; i++)
    {
        unsubscribed.push_back(i);
    }
    while (true)
    {
        //try subscribe with probability p
        if (subscribed.size() < consumers_num && uniform_real(gen) < subscribe_probability)
        {
            std::uniform_int_distribution<> subscribe_distribution(0, unsubscribed.size() - 1);
            auto index = subscribe_distribution(gen);
            auto value = unsubscribed[index];
            //std::cout << "size" << unsubscribed.size() << std::endl;
            //std::cout << "index" << index << std::endl;
            //std::cout << "subscribe " << value << std::endl;
            unsubscribed.erase(unsubscribed.begin() + index);

            subscribed.push_back(value);
        }

        //try unsubscribe with probability q
        if (subscribed.size() > 0 && uniform_real(gen) < unsubscribe_probability)
        {
            std::uniform_int_distribution<> unsubscribe_distribution(0, subscribed.size() - 1);
            auto index = unsubscribe_distribution(gen);
            auto value = subscribed[index];
            subscribed.erase(subscribed.begin() + index);
            unsubscribed.push_back(value);
        }

        std::cout << "subscribed: ";
        for (auto &s : subscribed)
        {
            std::cout << s << " ";
        }
        std::cout << std::endl;

        std::cout << std::endl;


        usleep(100'000);
    }*/