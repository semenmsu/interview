all: clean main

main:
	g++ -std=c++17 -O2 -o bin/main src/main.cpp -lpthread

fullpolling1:
	g++ -o bin/fullpolling1 src/fullpolling1.cpp -lpthread

producer-consumer: clean
	g++ -O2 -o bin/producer-consumer src/producer-consumer.cpp -lpthread 

clean:
	rm -rf bin/*