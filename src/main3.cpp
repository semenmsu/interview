//#include "producer.hpp"
//#include "consumer.hpp"
//#include "multiqueue1.hpp"
//#include "multiqueue2.hpp"
//#include "multiqueue3.hpp"
//#include "multiqueue4.hpp"
//#include "multiqueue5.hpp"
//#include "multiqueue6.hpp"
#include "multiqueue7.hpp"
#include <memory>
#include <thread>
#include <vector>
#include "utils.hpp"
#include "test/SimpleProducersConsumersScenario.hpp"

/*
void ProducersConsumersScenario(int32_t producers_num, int32_t consumers_num)
{
    printf("******************** new scenario  producers=%d  consumers=%d ******************** \n", producers_num, consumers_num);

    MultiQueueProcessor<int32_t, int32_t> multiQueueProsessor;
    std::vector<std::unique_ptr<Producer<int32_t, int32_t, decltype(multiQueueProsessor)>>> producers;
    std::vector<std::shared_ptr<Consumer<int32_t, int32_t>>> consumers;

    for (int i = 0; i < consumers_num; i++)
    {
        producers.push_back(std::make_unique<Producer<int32_t, int32_t, decltype(multiQueueProsessor)>>(multiQueueProsessor, i, 1, 1000));
        consumers.push_back(std::make_shared<Consumer<int32_t, int32_t>>(i));
        multiQueueProsessor.Subscribe(i, consumers[i]);
    }

    Wait(1);

    for (auto &producer : producers)
    {
        producer.get()->StartProducing();
    }

    Wait(NUM_SECS);

    AnalyzeConsumers(consumers);
}

//Изучение производительности перебором количества producer - consumer
void SimpleProducersConsumersScenario()
{
    for (int i = 1; i <= 30; i++)
    {
        ProducersConsumersScenario(i, i);
        std::cout << "\n"
                  << std::endl;
    }
}*/

template <typename Key, typename Value>
class ConsumersBag
{
public:
    ConsumersBag(MultiQueueProcessor<Key, Value> &mqp, int32_t max_consumers) : mqp_(mqp), max_consumers_(max_consumers)
    {
        for (int i = 0; i < max_consumers_; i++)
        {
            //consumers_.push_back(std::make_shared<Consumer<int32_t, int32_t>>(i));
            //consumers_[i] = std::make_shared<Consumer<int32_t, int32_t>>(i)
            consumers_.emplace(i, std::make_shared<Consumer<int32_t, int32_t>>(i));
        }
    }
    void Run()
    {
        for (int i = 0; i < max_consumers_; i++)
        {
            mqp_.Subscribe(i, consumers_[i]);
        }
        running_ = true;
        std::thread th(std::bind(&ConsumersBag::Process, this));
        th_ = std::move(th);
    }

    void Process()
    {
        while (running_)
        {
            sleep(1);
        }
    }

    void Analyze()
    {
        std::list<std::shared_ptr<Consumer<Key, Value>>> consumers;
        for (auto &kvp : consumers_)
        {
            consumers.push_back(kvp.second);
        }
        AnalyzeConsumers(consumers);
    }

    void Stop()
    {
        running_ = false;
    }

    ~ConsumersBag()
    {
        Stop();
        if (th_.joinable())
        {
            th_.join();
        }
    }

private:
    std::map<int32_t, std::shared_ptr<Consumer<Key, Value>>> consumers_;
    int32_t max_consumers_{0};
    MultiQueueProcessor<Key, Value> &mqp_;
    bool running_{false};
    std::thread th_;
};

//
void RandomSubscribeUnsusbscribeScenario(int32_t consumers_num = 5)
{
    MultiQueueProcessor<int32_t, int32_t> multiQueueProcessor;
    ConsumersBag<int32_t, int32_t> consumers(multiQueueProcessor, 5);

    std::vector<std::unique_ptr<Producer<int32_t, int32_t, decltype(multiQueueProcessor)>>> producers;

    for (int i = 0; i < consumers_num; i++)
    {
        producers.push_back(std::make_unique<Producer<int32_t, int32_t, decltype(multiQueueProcessor)>>(multiQueueProcessor, i, 1, 1000));
    }

    Wait(1);

    for (auto &producer : producers)
    {
        producer.get()->StartProducing();
    }
    consumers.Run();

    Wait(NUM_SECS);

    consumers.Analyze();
}

int main()
{
    SimpleProducersConsumersScenario();
    //RandomSubscribeUnsusbscribeScenario();
    return 0;
}